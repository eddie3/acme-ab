// Unsure where ID's are from, assuming loaded from an API
// So making them available here for now.
const FEATURE_ID_MAP = {
  largeButtons: "X",
  sortByPrice: "Y",
  showRecentlyViewed: "Z"
};
const getFeatureId = feature => {
  return FEATURE_ID_MAP[feature] || "unknown";
};

class AB {
  constructor() {
    this.queue = {};
    this._copyQueue();
  }

  _copyQueue() {
    if (
      window.AB &&
      Array.isArray(window.AB) &&
      typeof window.AB.push === "function"
    ) {
      window.AB.forEach(feature => {
        this.push(feature);
      });
    }
  }

  push(feature) {
    this.queue[feature] = getFeatureId(feature);
  }

  isEnabled(feature) {
    return Object.keys(this.queue).indexOf(feature) >= 0;
  }

  getEnabledVariantIds() {
    return Object.values(this.queue);
  }
}

module.exports = AB;
