const AB = require("./ab");

describe("AB", () => {
  beforeEach(function() {
    delete window.AB;
  });

  describe("init", () => {
    // Quick mock so we can inspect push calls without relying on
    // it's implementation, usually would just make a call to isEnabled here
    // but demonstrating TDD style.
    let oldPush;
    beforeEach(() => {
      oldPush = AB.prototype.push;
      AB.prototype.push = jest.fn();
    });
    afterEach(() => {
      AB.prototype.push = oldPush;
    });

    it("should use window.AB queue if already exists", () => {
      window.AB = window.AB || [];
      window.AB.push("largeButtons");
      window.AB.push("sortByPrice");
      window.AB.push("showRecentlyViewed");
      window.AB = new AB();
      expect(window.AB.push).toHaveBeenCalledWith("largeButtons");
      expect(window.AB.push).toHaveBeenCalledWith("sortByPrice");
      expect(window.AB.push).toHaveBeenCalledWith("showRecentlyViewed");
      expect(window.AB.push).toHaveBeenCalledTimes(3);
    });

    it("should not rely on window.AB queue if not specified", () => {
      delete window.AB;
      window.AB = new AB();
      window.AB.push("largeButtons");
      expect(window.AB.push).toHaveBeenCalledWith("largeButtons");
      expect(window.AB.push).toHaveBeenCalledTimes(1);
    });

    it("should ignore non array of window.AB and warn", () => {
      window.AB = "non-array-type";
      window.AB = new AB();
      expect(window.AB.push).not.toHaveBeenCalled();
    });
  });

  describe("push + isEnabled", () => {
    it("should push an AB feature", () => {
      const ab = new AB();
      ab.push("largeButtons");
      expect(ab.isEnabled("largeButtons")).toEqual(true);
    });

    it("isEnabled should return false when no feature", () => {
      const ab = new AB();
      expect(ab.isEnabled("nonexistent-test-feature")).toEqual(false);
    });

    it("should accept multiple calls with same args", () => {
      const ab = new AB();
      ab.push("largeButtons");
      ab.push("largeButtons");
      ab.push("largeButtons");
      expect(ab.isEnabled("largeButtons")).toEqual(true);
    });
  });

  describe("getEnabledVariantsIds", () => {
    it("should only return once ID per feature", () => {
      const ab = new AB();
      ab.push("largeButtons");
      ab.push("largeButtons");
      expect(ab.getEnabledVariantIds()).toEqual(["X"]);
    });

    it("should return multiple IDs", () => {
      const ab = new AB();
      ab.push("largeButtons");
      ab.push("sortByPrice");
      ab.push("showRecentlyViewed");
      expect(ab.getEnabledVariantIds()).toEqual(["X", "Y", "Z"]);
    });
  });
});
